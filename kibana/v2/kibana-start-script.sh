#!/bin/bash

export REPOSITORY_ELASTICSEARCH="https://bitbucket.org/meshstack/deployment-scripts-elasticsearch/raw/HEAD/kibana"

wget $REPOSITORY_ELASTICSEARCH/kibana-template.sh --no-cache
chmod +x kibana-template.sh
./kibana-template.sh -p evoila -e openstack