wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

sudo apt-get install apt-transport-https

echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-5.x.list

sudo apt-get update && sudo apt-get install kibana

sudo /bin/systemctl daemon-reload

sudo /bin/systemctl enable kibana.service

cat <<EOF > /etc/kibana/kibana.yml
server.host: "0.0.0.0"
server.maxPayloadBytes: 1048576
server.name: "$HOSTNAME"
elasticsearch.url: "http://$ELASTICSEARCH_ADDRESS:9200"
elasticsearch.preserveHost: true
kibana.index: ".kibana"
kibana.defaultAppId: "discover"
server.ssl.enabled: false
EOF
