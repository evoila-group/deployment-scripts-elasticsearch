#!/bin/bash

export REPOSITORY_ELASTICSEARCH="https://bitbucket.org/meshstack/deployment-scripts-elasticsearch/raw/elasticsearch5.3_elasticco/elasticsearch"

wget $REPOSITORY_ELASTICSEARCH/elasticsearch-template.sh --no-cache
chmod +x elasticsearch-template.sh
./elasticsearch-template.sh
