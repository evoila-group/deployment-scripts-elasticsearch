#!/bin/bash

export ELASTICSEARCH_MAJOR=5.3.0
export ELASTICSEARCH_VERSION=5.3.0
export ES_DATA=/data/elasticsearch

set -e

#accepts the Oracle JDK8 license automatically
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

#installs java8
apt-get update && apt-get install -y software-properties-common
yes \n | add-apt-repository ppa:webupd8team/java
apt-get update

apt-get install -y oracle-java8-installer
# automatically set up the Java 8 environment variables
apt-get install oracle-java8-set-default

# adds group and user for elasticsearch
groupadd -r elasticsearch
useradd -r -g elasticsearch elasticsearch

#adds directory for elasticseach data
mkdir -p $ES_DATA
chown -R elasticsearch:elasticsearch $ES_DATA

apt-get update

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

sudo apt-get install apt-transport-https

echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-5.x.list

# installs elasticsearch
sudo apt-get update && sudo apt-get install elasticsearch

sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable elasticsearch.service

sed -i "s|^#path.data:.*$|path.data: $ES_DATA|" "/etc/elasticsearch/elasticsearch.yml"
sed -i "s|^#network.host:.*$|network.host: 0.0.0.0|" "/etc/elasticsearch/elasticsearch.yml"
sed -i "s|^#http.port:.*$|http.port: 9200|" "/etc/elasticsearch/elasticsearch.yml"

sudo systemctl start elasticsearch.service

# wait for starting elasticsearch
sleep 30

# checks status of elasticsearch node
curl -XGET 'localhost:9200/?pretty'
