#!/bin/bash

export ES_DATA=/data/elasticsearch

#path used to check if service is installed
export CHECK_PATH=$ES_DATA/cluster-check

# start/stop commands for different environments
export OPENSTACK_START="systemctl start elasticsearch.service"
export OPENSTACK_STOP="systemctl stop elasticsearch.service"
export DOCKER_START="/usr/share/elasticsearch/bin/elasticsearch"
export DOCKER_STOP=""

#parameters for monit password and environment used for elasticsearch
usage() { echo "Usage: $0 [-e <string>]" 1>&2; exit 1; }

while getopts ":e:" o; do
    case "${o}" in
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${ENVIRONMENT}" ]; then
    usage
fi

# export for following scripts
export ENVIRONMENT=${ENVIRONMENT}

echo "environment = ${ENVIRONMENT}"
echo "repository_elasticsearch_cluster = ${REPOSITORY_ELASTICSEARCH_CLUSTER}"
echo "check_path = ${CHECK_PATH}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of elasticsearch
    chmod +x elasticsearch-cluster-run.sh
    ./elasticsearch-cluster-run.sh
else

    # loads and executes script for configuration of elasticsearch-cluster
    wget $REPOSITORY_ELASTICSEARCH_CLUSTER/elasticsearch-cluster-configuration.sh
    chmod +x elasticsearch-cluster-configuration.sh
    ./elasticsearch-cluster-configuration.sh

    # loads and executes script for startup of elasticsearch-cluster
    wget $REPOSITORY_ELASTICSEARCH_CLUSTER/elasticsearch-cluster-run.sh
    chmod +x elasticsearch-cluster-run.sh
    ./elasticsearch-cluster-run.sh
fi
