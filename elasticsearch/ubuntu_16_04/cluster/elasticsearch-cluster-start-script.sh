#!/bin/bash

export REPOSITORY_ELASTICSEARCH_CLUSTER="https://bitbucket.org/meshstack/deployment-scripts-elasticsearch/raw/HEAD/elasticsearch/cluster"

wget $REPOSITORY_ELASTICSEARCH_CLUSTER/elasticsearch-cluster-template.sh --no-cache
chmod +x elasticsearch-cluster-template.sh
./elasticsearch-cluster-template.sh -p evoila -e openstack
