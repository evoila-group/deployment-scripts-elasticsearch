#!/bin/bash

echo
echo "### Starting configuration of Elasticsearch ... ###"
echo

sed -i "s|^#path.data:.*$|path.data: $ES_DATA|" "/etc/elasticsearch/elasticsearch.yml"
sed -i "s|^#network.host:.*$|network.host: 0.0.0.0|" "/etc/elasticsearch/elasticsearch.yml"
sed -i "s|^#http.port:.*$|http.port: 9200|" "/etc/elasticsearch/elasticsearch.yml"

