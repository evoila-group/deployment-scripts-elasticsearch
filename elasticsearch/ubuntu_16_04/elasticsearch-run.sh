#!/bin/bash

echo
echo "### Starting elasticsearch-run.sh ... ###"
echo

echo "openstack_start = ${OPENSTACK_START}"
echo "docker_start = ${DOCKER_START}"
echo "environment = ${ENVIRONMENT}"
echo

# for openstack as environment
if [ "$ENVIRONMENT" = 'openstack' ]; then
  echo "Started for OpenStack environment."
  $OPENSTACK_START
fi

# wait for starting elasticsearch
sleep 30

# checks status of elasticsearch node
curl -XGET 'localhost:9200/?pretty'

echo
echo "  ┌────────────────────────────────────────────────────────────────┬─────────────────┐"
echo "  │   ┬ ┬ ┬ ┬ ┬ ┬    ╔══╗  ╦   ╦  ╔══╗  ╦  ╦    ╔══╗     ┌┬┐ ┌─┐   │                 │"
echo "  │   │││ │││ │││    ║═╣   ╚╗ ╔╝  ║  ║  ║  ║    ╠══╣      ││ ├┤    │   evoila GmbH   │"
echo "  │   └┴┘ └┴┘ └┴┘ o  ╚══╝   ╚═╝   ╚══╝  ╩  ╩══╝ ╩  ╩  o  ─┴┘ └─┘   │ Mainz / Germany │"
echo "  │                                                                │                 │"
echo "  │    ┌─┐┌─┐┬─┐┬  ┬┬┌─┐┌─┐  ┬┌─┐  ┌─┐┌┬┐┌─┐┬─┐┌┬┐┬┌┐┌┌─┐          │  www.evoila.de  │"
echo "  │    └─┐├┤ ├┬┘└┐┌┘││  ├┤   │└─┐  └─┐ │ ├─┤├┬┘ │ │││││ ┬          │ info@evoila.de  │"
echo "  │    └─┘└─┘┴└─ └┘ ┴└─┘└─┘  ┴└─┘  └─┘ ┴ ┴ ┴┴└─ ┴ ┴┘└┘└─┘  o o o   │                 │"
echo "  └────────────────────────────────────────────────────────────────┴─────────────────┘"

# for docker as environment
if [ "$ENVIRONMENT" = 'docker' ]; then
  $DOCKER_START
fi
