echo "path.repo: /media/es" >> /etc/elasticsearch/elasticsearch.yml

sudo apt-get install nfs-common -y

cd /media
mkdir es
sudo mount 172.16.248.159:/home/cf/es/backup /media/es/

sudo chmod u+w /media/es/
sudo chmod u+r /media/es/
sudo chmod u+x /media/es/
sudo chown -R elasticsearch:elasticsearch /media/es/
