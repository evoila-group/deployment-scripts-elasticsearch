#!/bin/bash

export REPOSITORY_ELASTICSEARCH="https://bitbucket.org/meshstack/deployment-scripts-elasticsearch/raw/HEAD/elasticsearch/es6_ubuntu_16_04"

wget $REPOSITORY_ELASTICSEARCH/elasticsearch-template.sh --no-cache
chmod +x elasticsearch-template.sh
./elasticsearch-template.sh -e openstack

