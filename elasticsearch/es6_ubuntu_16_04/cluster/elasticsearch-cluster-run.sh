#!/bin/bash

# restart elasticsearch with cluster configuration
systemctl restart elasticsearch.service

sleep 20

#check state of the cluster
curl -XGET 'http://localhost:9200/_cluster/state?pretty'