#!/bin/bash

export ELASTICSEARCH_MAJOR=5.3.0
export ELASTICSEARCH_VERSION=5.3.0
export PATH=/usr/share/elasticsearch/bin:$PATH
export ES_DATA=/data/elasticsearch

#path used to check if service is installed
export CHECK_PATH=$ES_DATA/

# start/stop commands for different environments
export OPENSTACK_START="systemctl start elasticsearch.service"
export OPENSTACK_STOP="systemctl stop elasticsearch.service"
# TODO: for docker not adapted for ubuntu 16.04, must be checked
export DOCKER_START="/usr/share/elasticsearch/bin/elasticsearch"
export DOCKER_STOP=""


#parameters for monit password and environment used for elasticsearch
usage() { echo "Usage: $0 [-e <string>]" 1>&2; exit 1; }

while getopts ":e:" o; do
    case "${o}" in
        e)
            ENVIRONMENT=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${ENVIRONMENT}" ]; then
    usage
fi

# export for following scripts
export ENVIRONMENT=${ENVIRONMENT}

echo "environment = ${ENVIRONMENT}"
echo "repository_elasticsearch = ${REPOSITORY_ELASTICSEARCH}"
echo "check_path = ${CHECK_PATH}"


# checks if service is installed
if [ -a $CHECK_PATH* ]; then
    # executes script for startup of elasticsearch
    chmod +x elasticsearch-run.sh
    ./elasticsearch-run.sh
else
    # loads and executes script for automatic installation of elasticsearch
    wget $REPOSITORY_ELASTICSEARCH/elasticsearch-install.sh --no-cache
    chmod +x elasticsearch-install.sh
    ./elasticsearch-install.sh

    # loads includes for the monit controlfile for this database
    mkdir -p /etc/monit.d/
    wget $REPOSITORY_ELASTICSEARCH/elasticsearch-monitrc -P /etc/monit.d/

    # loads and executes script for configuration of elasticsearch
    wget $REPOSITORY_ELASTICSEARCH/elasticsearch-configuration.sh --no-cache
    chmod +x elasticsearch-configuration.sh
    ./elasticsearch-configuration.sh

    # loads and executes script for startup of elasticsearch
    wget $REPOSITORY_ELASTICSEARCH/elasticsearch-run.sh --no-cache
    chmod +x elasticsearch-run.sh
    ./elasticsearch-run.sh
fi
